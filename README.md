# Liveliness Detection with Pose Estimation

This project implements a liveliness detection system using pose estimation. The system identifies various head movements and facial expressions to determine the liveliness of a person. The following actions are detected:

1. **Looking Left**: The system detects when the person is looking towards the left.
2. **Looking Right**: The system detects when the person is looking towards the right.
3. **Looking Up**: The system detects when the person is looking upwards.
4. **Looking Down**: The system detects when the person is looking downwards.
5. **Smile (Happy)**: The system detects when the person is smiling.
6. **Blink**: The system detects when the person blinks.

## How it Works

The liveliness detection system is implemented using pose estimation techniques. It relies on a combination of computer vision algorithms to analyze the movements and expressions of the person's face. Here's a brief overview of the process:

1. **Pose Estimation**: The system uses pose estimation to detect the key points on the person's face and estimate their head movements and orientation.
2. **Facial Landmark Detection**: Facial landmarks are identified to track the position of the eyes, nose, and mouth.
3. **Head Movement Detection**: By analyzing the changes in the positions of facial landmarks, the system determines the direction of the person's head movements (left, right, up, down).
4. **Smile Detection**: The system analyzes the shape of the mouth to detect when the person is smiling.
5. **Blink Detection**: By monitoring the position and movement of the eyelids, the system detects when the person blinks.

## Usage

To use the liveliness detection system, follow these steps:

1. Install the necessary dependencies "pip install -r requirements.txt" .
2. Run the main program (`main.py`).
3. Ensure that the camera is properly positioned to capture the person's face.
4. The system will display real-time feedback indicating the detected actions (looking left, looking right, etc.).

## Model Performance

![looking_left](looking_left.jpg)
![looking_right](looking_right.jpg)
![looking_up](looking_up.jpg)
![looking_down](looking_down.jpg)
![happy](happy.jpg)
![blink](blink.jpg)


Above is an example image showcasing the capabilities of the trained models in accurately detecting people's movements.

## References

- [MediaPipe](https://mediapipe.dev/)
- [OpenCV](https://opencv.org/)


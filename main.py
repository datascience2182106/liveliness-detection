import cv2
import mediapipe as mp
import numpy as np
import time
import math
from collections import Counter


def blinking(face_landmarks):
    # LEFT EYE
    pt_left_bottom = face_landmarks.landmark[374]
    x_lb = int(pt_left_bottom.x * img_w)
    y_lb = int(pt_left_bottom.y * img_h)
    pt_left_top = face_landmarks.landmark[386]
    x_lt = int(pt_left_top.x * img_w)
    y_lt = int(pt_left_top.y * img_h)
    pt_left_left = face_landmarks.landmark[382]
    x_ll = int(pt_left_left.x * img_w) 
    y_ll = int(pt_left_left.y * img_h)
    pt_left_right = face_landmarks.landmark[249]
    x_lr = int(pt_left_right.x * img_w)
    y_lr = int(pt_left_right.y * img_h)

    # RIGHT EYE
    pt_right_top = face_landmarks.landmark[159]
    x_rt = int(pt_right_top.x * img_w)
    y_rt = int(pt_right_top.y * img_h)
    pt_right_bottom = face_landmarks.landmark[145]
    x_rb = int(pt_right_bottom.x * img_w)
    y_rb = int(pt_right_bottom.y * img_h)
    pt_right_right = face_landmarks.landmark[173]
    x_rr = int(pt_right_right.x * img_w)
    y_rr = int(pt_right_right.y * img_h)
    pt_right_left = face_landmarks.landmark[7]
    x_rl = int(pt_right_left.x * img_w)
    y_rl = int(pt_right_left.y * img_h)

    # Finding distances
    left_vertical = math.dist((x_lt, y_lt), (x_lb, y_lb))
    left_horizontal = math.dist((x_lr, y_lr), (x_ll, y_ll))
    right_vertical = math.dist((x_rt, y_rt), (x_rb, y_rb))
    right_horizontal = math.dist((x_rl, y_rl), (x_rr, y_rr))

    # Finding EAR(Eye aspect ratio)
    ear_left = left_vertical / left_horizontal
    ear_right = right_vertical / right_horizontal

    if ear_left < 0.27 and ear_right < 0.27:
        is_blink = True
    else:
        is_blink = False
                
    return is_blink

def get_happiness_score(face_landmarks):
    left_eye = (int(face_landmarks.landmark[33].x * img_w), int(face_landmarks.landmark[33].y * img_h))
    right_eye = (int(face_landmarks.landmark[263].x * img_w), int(face_landmarks.landmark[263].y * img_h))
    mouth_left = (int(face_landmarks.landmark[61].x * img_w), int(face_landmarks.landmark[61].y * img_h))
    mouth_right = (int(face_landmarks.landmark[291].x * img_w), int(face_landmarks.landmark[291].y * img_h))
    mouth_center = (int((mouth_left[0] + mouth_right[0]) / 2), int((mouth_left[1] + mouth_right[1]) / 2))
                
    # Calculating the distance between mouth corners using Euclidean distance
    mouth_width = np.linalg.norm(np.array(mouth_left) - np.array(mouth_right))
                
    # Calculating the distance between the mid-point of the eyes and mid-point of the mouth
    eye_distance = np.linalg.norm(np.array(left_eye) - np.array(right_eye))
    mouth_eye_distance = np.linalg.norm(np.array(mouth_center) - np.array(((left_eye[0] + right_eye[0]) // 2, (left_eye[1] + right_eye[1]) // 2)))
                
    # Computing happiness score
    happiness_score = (mouth_width / eye_distance) / (mouth_eye_distance / eye_distance)

    return happiness_score

    

num = 1

mp_face_mesh = mp.solutions.face_mesh
face_mesh = mp_face_mesh.FaceMesh(min_detection_confidence=0.5, min_tracking_confidence=0.5, max_num_faces=1)

mp_drawing = mp.solutions.drawing_utils

drawing_spec = mp_drawing.DrawingSpec(thickness=1, circle_radius=1, color=(0,255,0))

is_blink = False

tasks = ["Move_your_head_left", "Move_your_head_right", "Move_your_head_up", "Move_your_head_down", "Smile", "Close_your_ice"]
actions_list = ["Looking Left", "Looking Right", "Looking Up", "Looking Down", "Smile", "Blink"]

cap = cv2.VideoCapture(0)

# frame_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
# frame_height = int(c ap.get(cv2.CAP_PROP_FRAME_HEIGHT))
# out = cv2.VideoWriter('output_video.avi', cv2.VideoWriter_fourcc(*'DIVX'), 20, (frame_width, frame_height))

times = []

actions = []

is_wrong = False

while cap.isOpened():
    if len(actions_list) == 0:
        break

    success, image = cap.read()

    start = time.time()

    times.append(start)

    image = cv2.cvtColor(cv2.flip(image, 1), cv2.COLOR_BGR2RGB)

    image.flags.writeable = False
    
    results = face_mesh.process(image)
    
    image.flags.writeable = True
    
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

    if is_wrong:
        cv2.putText(image, "Wrong Action try again!", (50, 150), cv2.FONT_HERSHEY_COMPLEX, 1.0, (0, 0, 255), 2)

    cv2.putText(image, " ".join(tasks[0].split("_")), (50, 200), cv2.FONT_HERSHEY_COMPLEX, 1.0, (0, 255, 0), 2)

    img_h, img_w, img_c = image.shape
    face_3d = []
    face_2d = []

    if results.multi_face_landmarks:
        for face_landmarks in results.multi_face_landmarks:
            for i, lm in enumerate(face_landmarks.landmark):
                if i == 33 or i == 263 or i == 1 or i == 61 or i == 291 or i == 199:
                    if i == 1:
                        nose_2d = (lm.x * img_w, lm.y * img_h)
                        nose_3d = (lm.x * img_w, lm.y * img_h, lm.z * 3000)

                    x, y = int(lm.x * img_w), int(lm.y * img_h)

                    face_2d.append([x, y])

                    face_3d.append([x, y, lm.z])       
            
            face_2d = np.array(face_2d, dtype=np.float64)

            face_3d = np.array(face_3d, dtype=np.float64)

            focal_length = 1 * img_w

            camera_matrix = np.array([ [focal_length, 0, img_h / 2],
                                    [0, focal_length, img_w / 2],
                                    [0, 0, 1]])

            distortion_matrix = np.zeros((4, 1), dtype=np.float64)

            # estimating the pose of the object in the scene
            success, rot_vec, trans_vec = cv2.solvePnP(face_3d, face_2d, camera_matrix, distortion_matrix)
            
            # The rotation matrix represents the rotation transformation from the object's local coordinate system to the camera coordinate system
            rotation_matrix, jacobian_matrix = cv2.Rodrigues(rot_vec)

            # camputing angles of rotation matrix
            angles, mtxR, mtxQ, Qx, Qy, Qz = cv2.RQDecomp3x3(rotation_matrix)

            start = time.time()

            x = angles[0] * 360
            y = angles[1] * 360
            z = angles[2] * 360
          
            # Detecting happiness
            happiness_score = 0
            for face_landmarks in results.multi_face_landmarks:
                is_blink = blinking(face_landmarks)
                happiness_score = get_happiness_score(face_landmarks)
            
            # Adjust the threshold for happiness detection
            if happiness_score > 0.8:
                emotion_text = "Happy"
            else:
                emotion_text = "Not Happy"

            if y < -5:
                text = "Looking Left"
            elif y > 5:
                text = "Looking Right"
            elif x < -5:
                text = "Looking Down"
            elif x > 5:
                text = "Looking Up"
            else:
                text = "Forward"

            nose_3d_projection, jacobian = cv2.projectPoints(nose_3d, rot_vec, trans_vec, camera_matrix, distortion_matrix)

            p1 = (int(nose_2d[0]), int(nose_2d[1]))
            p2 = (int(nose_2d[0] + y * 10) , int(nose_2d[1] - x * 10))
            

            if text == "Forward":
                if times[-1] - times[0] > 1:
                    is_wrong = False

                if is_blink:
                    actions.append("Blink")
                    # cv2.putText(image, "Blink: True", (20, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

                    if times[-1] - times[0] > 3:
                        times = []
                        real_action = Counter(actions).most_common(1)[0][0]

                        if actions_list[0] == real_action:
                            actions_list.remove(actions_list[0])
                            tasks.remove(tasks[0])
                            actions = []

                        else:
                            is_wrong = True
                            actions = []



                elif emotion_text == "Happy":

                    actions.append("Smile")
                    # cv2.putText(image, "Emotion: " + emotion_text, (20, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

                    if times[-1] - times[0] > 3:
                        times = []
                        real_action = Counter(actions).most_common(1)[0][0]

                        if actions_list[0] == real_action:
                            actions_list.remove(actions_list[0])
                            tasks.remove(tasks[0])
                            actions = []
                            # is_wrong = False
                        else:
                            is_wrong = True

                            actions = []
            else:

                actions.append(text)
                if times[-1] - times[0] > 1:
                    is_wrong = False

                # cv2.putText(image, text, (20, 50), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 2)

                if times[-1] - times[0] > 3:
                    
                    times = []
                    real_action = Counter(actions).most_common(1)[0][0]

                    if actions_list[0] == real_action:
                        actions_list.remove(actions_list[0])
                        tasks.remove(tasks[0])
                        actions = []

                    else:
                        is_wrong = True
                        actions = []

        mp_drawing.draw_landmarks(
                    image=image,
                    landmark_list=face_landmarks,
                    connections=mp_face_mesh.FACEMESH_CONTOURS,
                    landmark_drawing_spec=drawing_spec,
                    connection_drawing_spec=drawing_spec)

    # out.write(image)
    cv2.imshow('Head Pose Estimation', image) 

    if cv2.waitKey(1) == ord("q"):
        break


cap.release()
cv2.destroyAllWindows()
cv2.waitKey(1)

